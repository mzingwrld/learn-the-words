import React from 'react';
import s from './TranslatePopup.module.scss';
import cl from 'classnames';
import {CloseCircleOutlined, PlusCircleOutlined } from '@ant-design/icons';
import hash from 'object-hash';

let popupX = null;
let popupY = null;

class PopupTranslate extends React.Component {
    state = {
        translationparsed: [],
        prevword: null,
        prevTranslationHash: null,
    }

    componentDidMount() {
        const {onComponentDidMountCustom} = this.props;
        let clientWidth = document.getElementById('translatePopup').clientWidth ;
        let clientHeight = document.getElementById('translatePopup').clientHeight;
        popupX = clientWidth;
        popupY = clientHeight;
        onComponentDidMountCustom (popupX, popupY); /* send dimensions of popup to parent */
    }

    componentDidUpdate () {
        const { prevTranslationHash } = this.state;
        const { translation, word, handleUpdatePopupPositionTrigger, handleMovePopupOutsideTrigger } = this.props;
        
        let clientWidth = document.getElementById('translatePopup').clientWidth ;
        let clientHeight = document.getElementById('translatePopup').clientHeight;

        

        /* TODO: call it only after map finished its work */
        let newHash = hash(translation);

        //console.log("#### TranslatePopup translation:", translation);
        //console.log("#### TranslatePopup newHash:", newHash);
        //console.log("#### TranslatePopup prevTranslationHash:", prevTranslationHash);
        //console.log("#### TranslatePopup clientWidth:", clientWidth);
        //console.log("#### TranslatePopup clientHeight:", clientHeight); 
        if (translation === 'nullg') { 
            handleMovePopupOutsideTrigger();
            if(prevTranslationHash !== hash('nullg')){
                this.setState( () => {
                    return {
                        prevTranslationHash: hash('nullg'),
                    }
                });
            }
        } 
        if(translation && translation!== 'nullg') { /* Если вернулся массив (даже пустой) */
            if (prevTranslationHash !== newHash) { /* Если хэш предыдущего объекта != текущему */
                    this.renderTranslationContent(translation).then( res => {
                        //console.log("#### TRANSLATION OBJ:", res);

                        this.setState( () => {
                            return {
                                translationparsed: res,
                                prevword: word,
                                prevTranslationHash: hash(translation),
                            }
                        });
                        
                            clientWidth = document.getElementById('translatePopup').clientWidth;
                            clientHeight = document.getElementById('translatePopup').clientHeight;
                            //console.log("#### TranslatePopup componentDidUpdate before handleUpdatePopupPositionTrigger");
                            handleUpdatePopupPositionTrigger(clientWidth, clientHeight);

                    })
            }
        }
    }

    handleMakeFirstLetterCapital = (w) => {
        let result;
        if (w.word && w.word.length > 1) {
            result = w.word.charAt(0).toUpperCase() + 
            w.word.toString().slice(1); 
        }
        else { 
            result = w.word;
        }
        return result || null;
    }

    handleTranslatePartsOfSpeech = (w) => {
        /**
         * TODO:
         * Сделать подсказки для каждой части речи (для определителя точно нужно)
         */
        switch (w) {
            case 'noun': return 'Существительное';
            case 'adjective': return 'Прилагательное';
            case 'verb': return 'Глагол';
            case 'preposition': return 'Предлог';
            case 'adverb': return 'Наречие';
            case 'participle': return 'Причастие';
            case 'conjunction': return 'Союз';
            case 'pronoun': return 'Местоимение';
            case 'particle': return 'Частица';
            case 'numeral': return 'Имя числительное';
            case 'determiner': return 'Определитель';
            default: return 'Прочее';
        }
    }

    renderTranslationContent = async (w) => {

        if (w.length !== 0 && w!=='nullg') {
            return (
                w.map ( (tr, index) => {
                    if (tr.tr) {
                        return <ul className={s.list} key={index}> <div className={s.listTitle}>{this.handleTranslatePartsOfSpeech(tr.pos)} </div>
                            {
                                tr.tr.map( (word, idx) => {
                                    if (idx < 3) { /* Output only 3 first words for one part of speech */
                                        return <li key={idx} className={s.listChild}>{word.text} <div className={s.iconAdd}><PlusCircleOutlined /></div> </li> 
                                    }
                                })
                            }
                        </ul>
                    }
                }) 
            )
        }
        else {
            return (
                    <>
                    <div>Прошу прощения. Это бета версия приложения.</div>
                    <div>API Yandex.Translate v.1 не умеет переводить слова в множественном числе, некоторые составные слова.</div>
                    <div>Скоро мы перейдем на вторую версию, которая позволит переводить всё что угодно. И даже предложения.</div>
                    </>
            )
        }
    }

    render() {
        const { showXpos, showYpos, show, onClickCustom, word } = this.props;
        let popupStyle = {
          left: showXpos,
          top: showYpos,
        };
        //console.log("#### translatePopup showXpos", showXpos);
        //console.log("#### translatePopup showYpos", showYpos);
        return (
        <div 
            id='translatePopup' 
            className={ cl(s.popupMain, { 
                                [s.show] : show, 
                            }
                        )}
            style={popupStyle}
        >
          <div className={s.popupContent}>
              <div className={s.popupInner}>
                {/* <div className={s.popupMenu}><div className={s.iconClose}><CloseCircleOutlined onClick={() => {onClickCustom()}}/></div></div> */}
                <div className={s.popupTitle}> {this.handleMakeFirstLetterCapital({word})} </div>
                <div className={s.popupContent}>
                        {   
                            this.state.translationparsed
                        }
                </div>
              </div>
          </div>
        </div>
        )
    }
}

export default PopupTranslate;