import React, { Component } from 'react';
import s from './Account.module.scss';
import { Layout } from 'antd';
import FirebaseContext from '../../context/firebaseContext';
//import { firedb } from '../../services/firebase';

const { Header } = Layout;

class AccountPanel extends Component {

    handeLogout = () => {
        const {auth} = this.context;
        auth.signOut()
        .then(res => {
            console.log("####: logged out ");
            //history.push('/login'); /* Causes state change on unmounted component */
        })
    }

    handleEmailVerification = () => {
        const {sendVerificationEmail} = this.context;
        
        sendVerificationEmail().then(res => {
            console.log("#### verification sent", res);
        })
        .catch(error => {
            console.log("#### verification email not sent");
        })
    }

    render() {
        //console.log("#### account panel props.props:", this.props.props);
        //console.log("#### account panel props:", this.props);
        
        return (
            <>
                <Layout>
                    <Header>
                        <div className={s.auth}>
                                Пользователь: { this.props.useremail } 
                                <div className={s.divider}>/</div>
                            <div className={s.logout}
                                onClick={this.handleEmailVerification}
                            >
                                Подтвердите email
                            </div>
                            <div className={s.divider}>
                                /
                            </div>                            
                            <div className={s.logout}
                                onClick={this.handeLogout}
                            >
                                Выйти
                            </div>
                        </div>
                        
{/*                         <Menu theme="light" mode="horizontal">

                            <Menu.Item key="1">nav 1</Menu.Item>
                            <Menu.Item key="2">nav 2</Menu.Item>
                            <Menu.Item key="3">nav 3</Menu.Item>
                        </Menu> */}
                    </Header>
                </Layout>
            </>
        );
    }
}
AccountPanel.contextType = FirebaseContext;

export default AccountPanel;