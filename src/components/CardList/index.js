import React, {Component} from 'react';
import Card from '../Card';
import s from './CardList.module.scss';
import cl from 'classnames';
import FlexBoxContainer from '../FlexBoxContainer';
import {RollbackOutlined, SwapOutlined, LoadingOutlined} from '@ant-design/icons';
import {Input} from 'antd';
import getTranstalateWord from '../../services/translate.js';


//import TestContext from '../../context/testContext';

const {Search} = Input;
const inputPlaceholderEng = 'Впиши новое слово (англ.)';
const inputPlaceholderRus = 'Впиши новое слово (рус.)';
class CardList extends Component {
    state = {
        value : '',
        label : '',
        alert : false,
        change : false,
        isBusy: false,
        lang: 'en-ru',
        inputplaceholder: inputPlaceholderEng,
        langfrom: 'Английский',
        langto: 'Русский',
        alerttext: '',
    }

    handleInputChange = (e) => {
        //console.log("####: value ", e.target.value);
        this.setState(
            {
                value: e.target.value
            }
        )
    }

    checkForDuplicates = (word1, word2) => {
        const { item = []} = this.props;
        let checkRes = null;
        console.log("####: words:", word1, ' / ', word2);
        if (this.checkIfStringCyrillic(word1)) {
            checkRes = item.find(o => o.eng.toLowerCase() === word2.toLowerCase() && o.rus.toLowerCase() === word1.toLowerCase());
        }
        else {
            checkRes = item.find(o => o.eng.toLowerCase() === word1.toLowerCase() && o.rus.toLowerCase() === word2.toLowerCase());
        }
        return checkRes;
    }

    checkIfStringCyrillic = (str) => {
        const russian = /^[а-яё -]+$/;
        return russian.test(str);
    }

    getWord = async () => {
        try {
            const {lang, value} = this.state;

            const inputValue = value.toLowerCase();
            const getWord = await getTranstalateWord(inputValue, lang);
            console.log("####: translate: ", getWord);
            const translatedWord = getWord[0].tr[0].text;
            console.log("#### DUPLICATES CHECK:", this.checkForDuplicates(inputValue, translatedWord));
            if (!this.checkForDuplicates(inputValue, translatedWord)) {
                this.setState( () => {
                    this.props.onAddItem(inputValue, translatedWord, lang);
                    return {
                        value : '',
                        label : `${inputValue} / ${translatedWord}`,
                        alert: false,
                        change: true,
                        isBusy: false, /* Hide loader */
                    }
                })
            }
            else {
                this.setState( () => {
                    return {
                        alert: true, /* Show alert */
                        alerttext: 'Такая карточка уже добавлена',
                        isBusy: false, /* Hide loader */
                        change: false, /* Hide loader */
                    }
                });
                //let timeOut = 
                setTimeout(() => {
                    this.setState({
                        alert: false,
                    });
                }, 2000);
            }
        }
        catch {
            this.setState ( () => {
                return {
                    alert: true,
                    alerttext: 'Мы не нашли такого слова',
                    isBusy: false, /* Hide loader */
                    change: false, /* Hide loader */
                }
            });
            //let timeOut = 
            setTimeout(() => {
                this.setState({
                    alert: false,
                });
            }, 2000);
        }
    }

    handleSubmitForm = async() => {
        /**
         * TODO: Валидировать английское слово введено или русское
         * Status: Done
         */
        const {value, langfrom} = this.state;

        let isCyrillic = this.checkIfStringCyrillic(value);
        let translateDirection;
        if (isCyrillic && langfrom === 'Русский') {
            translateDirection = langfrom;
        } else if (!isCyrillic && langfrom === 'Английский') {
            translateDirection = langfrom;
        }

        // console.log("####: translateDirection", translateDirection);

        if (value !== '') {
            if (translateDirection===langfrom) {
                this.setState( {
                    isBusy: true, /* Show loader */
                }, this.getWord)
            } else {
                this.setState( () => {
                    return {
                        alert: true, /* Show alert */
                        alerttext: 'Введите слово в верной раскладке',
                    }
                });
                //let timeOut = 
                setTimeout(() => {
                    this.setState({
                        alert: false,
                    });
                  }, 2000);
            }
        }
        else {
            this.setState( () => {
                return {
                    alert: true, /* Show alert */
                    alerttext: 'Введите слово',
                }
            });
            //let timeOut = 
            setTimeout(() => {
                this.setState({
                    alert: false,
                });
              }, 2000);
        }
    }

    handleLanguagueSwap = () => {
        const {langfrom, langto} = this.state;
        let tmp = langfrom;
        let tmp2 = langto;
        let placeholdertmp;
        let langtmp;

        if (tmp2 === 'Английский') {
            placeholdertmp = inputPlaceholderEng;
            langtmp = 'en-ru';
        }
        else {
            placeholdertmp = inputPlaceholderRus;
            langtmp = 'ru-en';
        }
        this.setState( () => {
            return {
                langfrom: tmp2,
                langto: tmp,
                inputplaceholder: placeholdertmp,
                lang: langtmp
            }
        })
    }

    render() {
        const { item = [], onDeletedItem, onRevertChange, loading} = this.props;
        const { alert, change, isBusy, alerttext } = this.state;
        //console.log("#### ITEM: ", item);
        //console.log("####: Cardlist props", this.props);
        let reversedItem = item.slice(0).reverse();
        return (
            <>  
                <FlexBoxContainer directionCol>
                    <div className={s.newword}>
                        <div className={s.label}> 
                            { this.state.label }
                        </div>
                        <div className={cl(s.icons)} onClick={this.handleLanguagueSwap}>
                            {this.state.langfrom} <SwapOutlined /> {this.state.langto}
                        </div>
                        <div className={s.form}>
                            <Search className={s.antcustom}
                                placeholder={this.state.inputplaceholder}
                                enterButton="Добавить"
                                size="large"
                                value={this.state.value}
                                loading={isBusy}
                                onChange={this.handleInputChange}
                                onSearch={this.handleSubmitForm}
                            />                            
                            <div className={
                                cl(s.alert, {
                                    [s.done] : alert
                                })
                            }>
                                {alerttext}
                            </div>
                        </div>
                        
                        <div className={
                            cl(s.loading, {
                            [s.done] : loading
                            })
                        }
                        >
                            <LoadingOutlined spin />
                        </div>
                        
                        <div className={
                                cl(s.iconCancel, {
                                [s.show] : change
                                })
                            }
                            onClick={() => {
                                onRevertChange();
                                this.setState( () => {
                                    return {
                                        change: false,
                                        label : '', 
                                }
                                })
                            }}
                        >
                            <RollbackOutlined /> Отменить
                        </div>
                    </div>
                </FlexBoxContainer>
                <FlexBoxContainer directionRow>
                {
                    reversedItem.map( ( {eng, rus, id} ) => (
                        <Card 
                            onDeleted={() => {
                                //console.log("####: 2 level");
                                onDeletedItem(id);
                                this.setState( () => {
                                    return {
                                        change: true
                                    }
                                })
                            }}
                            key = {id}
                            eng = {eng}
                            rus = {rus}
                        />
                    ))
                }
                </FlexBoxContainer>
            </>
        )
    }
}

//CardList.contextType = TestContext;

export default CardList;