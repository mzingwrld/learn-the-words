import React from 'react';
import cl from 'classnames';
import { CheckSquareOutlined, DeleteOutlined } from '@ant-design/icons';

import s from './Card.module.scss';

 class Card extends React.Component {

    state = {
        done: false,
        isRemembered: false,
    }

    handlerCardClick = () => {
        this.setState( ({done, isRemembered}) => {
            if (!isRemembered) {
                return {
                    done: !done
                }
            }
            else return {}
        });

    }
    handlerIsRememberClick = () => {
        this.setState( ({done, isRemembered}) => {
            if(done) {
                return {
                    isRemembered: !isRemembered,
                }
            }
            else {
                return {
                isRemembered: !isRemembered,
                done: !done
                }
            }
        });

    }    

    handlerDeletedClick = () => {
        //console.log("####: 1 level");
        this.props.onDeleted();
    }

    render () {
        const {rus, eng} =  this.props;
        const {done, isRemembered} = this.state;

        return (
                <div className= { cl(s.root)}>
                    <div 
                        className={ cl(s.card, { 
                            [s.done] : done , 
                            [s.isRemembered] : isRemembered 
                        }) }
                        onClick={this.handlerCardClick}
                    >
                        <div className={s.cardInner}>
                            <div className={s.cardFront}>
                                { eng }
                            </div>
                            <div className={s.cardBack}>
                                { rus }
                            </div>
                            
                        </div>
                    </div>
                    <div className= {cl(s.icons, { [s.active] : isRemembered })}>
                            <CheckSquareOutlined onClick={this.handlerIsRememberClick}/>
                    </div>
                    <div className= {cl(s.icons)}>
                            <DeleteOutlined onClick={this.handlerDeletedClick}/>
                    </div>                    
                </div>
        );
    }
} 

const hoc = (Component) =>  {
    return class extends React.PureComponent {
        state = {
            done: false,
            isRemembered: false,
        }
    
        handlerCardClick = () => {
            this.setState( ({done, isRemembered}) => {
                if (!isRemembered) {
                    return {
                        done: !done
                    }
                }
                else return {}
            });
    
        }
        handlerIsRememberClick = () => {
            this.setState( ({done, isRemembered}) => {
                if(done) {
                    return {
                        isRemembered: !isRemembered,
                    }
                }
                else {
                    return {
                    isRemembered: !isRemembered,
                    done: !done
                    }
                }
            });
    
        }    
    
        handlerDeletedClick = () => {
            //console.log("####: 1 level");
            this.props.onDeleted();
        }
    
        render () {
            const {rus, eng} =  this.props;
            const {done, isRemembered} = this.state;
    
            return (
                    <div className= { cl(s.root)}>
                        <div 
                            className={ cl(s.card, { 
                                [s.done] : done , 
                                [s.isRemembered] : isRemembered 
                            }) }
                            onClick={this.handlerCardClick}
                        >
                            <div className={s.cardInner}>
                                <div className={s.cardFront}>
                                    { eng }
                                </div>
                                <div className={s.cardBack}>
                                    { rus }
                                </div>
                                
                            </div>
                        </div>
                        <div className= {cl(s.icons, { [s.active] : isRemembered })}>
                                <CheckSquareOutlined onClick={this.handlerIsRememberClick}/>
                        </div>
                        <div className= {cl(s.icons)}>
                                <DeleteOutlined onClick={this.handlerDeletedClick}/>
                        </div>                    
                    </div>
            );
        }
    };
}

export default hoc(Card);
//export default Card;