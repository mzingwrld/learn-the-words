import React from 'react';
import s from './Footer.module.scss';

const Footer = ({children}) => {
    return (
        <div className={s.footer}>
            {children}

            {/* <div className={s.container}>
                <div className={s.box}>Menu 1</div>
                <div className={s.box}>Menu 2</div>
                <div className={s.box}>Menu 3</div>
                <div className={s.box}>Menu 4</div>
            </div>  */}
            <div className={s.author}>Ivan Chervonyy @ React-marathon 2020</div>
        </div>
    );
}

export default Footer;