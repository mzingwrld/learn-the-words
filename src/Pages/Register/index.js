import React, {Component} from 'react';
import { Form, Input, Button } from 'antd';
//import { Checkbox} from 'antd';
import s from './Register.module.scss';
//import { firedb } from '../../services/firebase';
import cl from 'classnames';
//import { HashRouter as Router, Route, Switch, Link, withRouter } from 'react-router-dom';
//import { Breadcrumb, Alert } from 'antd';
import FirebaseContext from '../../context/firebaseContext';

/**
 * This component can proceed registration user
 */

class RegisterPage extends Component {
    state = {
        alertshow : false,
    }
     
    onFinish = ({email, password}) => {
        const {registerWithEmail} = this.context;
        const {history} = this.props.props;
        console.log('####: Success login');

        registerWithEmail(email, password)
        .then(res => {
            console.log("####: res ", res);
            history.push('/home');

            /*
             // To do: send verification email
             // Not working -> test in in netlify

             this.props.sendVerificationEmail().then(res => {
                console.log("#### verification sent");
                history.push('/home');
            }) 
            .catch(error => {
                console.log("#### verification email not sent");
                
                 // ToDo: Handle verification later in account settings, for example
                 
            });
            */

        })
        .catch(error =>{
            console.log("#### error:", error.code);
            if(error.code === 'auth/email-already-in-use') {
                this.setState({
                        alertshow: true,
                });
            }
        });
    }
    
    onFinishFailed = errorInfo => {
        console.log('####: Failed:', errorInfo);
    };

    renderForm = () => {
        const layout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
        };

        const tailLayout = {
            wrapperCol: { offset: 6, span: 18 },
        };
        const {show} = this.props;
        const {alertshow} = this.state;
        console.log("####: show register form:" , show);
        return (
            <Form
                {...layout}
                name="Register"
                initialValues={{ remember: true }}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
            >
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[{ required: true, message: 'Please input your email!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Пароль"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password />
                </Form.Item>

                {/* <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                    <Checkbox>Remember me</Checkbox>
                </Form.Item> */} 

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Зарегистрироваться
                    </Button>
                </Form.Item>

                <div className={cl(s.alert, {[s.show] : alertshow})}>Пользователь с таким email уже зарегистрирован!</div>
            </Form>
        )
    }

    render () {
        //console.log("####: props of in register page:", this.props.props);
        return (
            <div className={cl(s.root, {
                [s.show] : this.props.show
            })}>
                <div className={s.formWrapper}>
                    { /* <div className={s.choice}>
                        Зарегистрироваться
                    </div> */ }
                    <div className={s.formFields}>
                        {this.renderForm()}
                    </div>
                </div>    
            </div>
        );
    }
}

RegisterPage.contextType = FirebaseContext;

export default RegisterPage;