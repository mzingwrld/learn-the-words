import React, {Component} from 'react';
//import { HashRouter as Router, Route, Switch, Link, withRouter } from 'react-router-dom';
import { Layout } from 'antd';
import LoginPage from '../Login';
import RegisterPage from '../Register';
import s from './Auth.module.scss';
import cl from 'classnames';
//import { FacebookFilled } from '@ant-design/icons';
const {  Content } = Layout;

class AuthSwitch extends Component {
    state = {
        showregistration: false,
        showlogin: true,
    }

    handleShowLogin = () => {
        this.setState(() => {
            return {
                showlogin: true,
                showregistration: false,
            }
        })
    }

    handleShowRegistration = () => {
        this.setState(() => {
            return {
                showlogin: false,
                showregistration: true,
            }
        })
    } 


    render () {
        //console.log("####: props of router in authSwitch page:", this.props);
        return (
    <Layout>
        <Content> 
        <div className={s.root}>
            <div className={s.nav}>
                { /* <div className={
                        cl(s.menu)}
                        onClick={this.handleShowLogin}
                >
                        Войти
                </div> */}
                    
                    <div className={
                        cl(s.menu, {
                        [s.active] : this.state.showlogin
                        })}
                        onClick={this.handleShowLogin}
                    >
                        Войти
                    </div>
                    <span className={s.divider}> Нет аккаунта?</span>
                    <div className={
                        cl(s.menuRegister, {
                        [s.active] : this.state.showregistration
                        })}
                        onClick={this.handleShowRegistration}
                    >
                        Зарегистрироваться
                    </div>
            </div>
            <div className={s.form}>
                <LoginPage show={this.state.showlogin} props={this.props}/>
                <RegisterPage show={this.state.showregistration } props={this.props}/>
            </div>
        </div>
        </Content> 
    </Layout> 
       
    );
}};

export default AuthSwitch;