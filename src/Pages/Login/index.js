import React, {Component} from 'react';
import { Form, Input, Button } from 'antd';
//import { Checkbox } from 'antd';
import s from './Login.module.scss';
//import { firedb } from '../../services/firebase';
import cl from 'classnames';    

//import { HashRouter as Router, Route, Switch, Link, withRouter } from 'react-router-dom';
//import { Breadcrumb, Alert } from 'antd';
import FirebaseContext from '../../context/firebaseContext';


/**
 * This component can proceed registration & authentication of user
 */

class LoginPage extends Component {
    state = {
        alertshow : false,
    }

    onFinish = ({email, password}) => {
        const {signInWithEmail} = this.context;
        const {history} = this.props.props;
        //const {alertshow} = this.state;
        console.log("####: history in login page:", history);
        console.log('####: Success login');
        signInWithEmail(email, password)
            .then(res => {
                console.log("####: res ", res);
                history.push('/home');
            })
            .catch(error => {
                console.log("#### error:", error.code);
                if(error.code === 'auth/user-not-found') {
                    this.setState({
                            alertshow: true,
                    });
                }
                
            })
    }
    
    onFinishFailed = errorInfo => {
        console.log('####: Failed:', errorInfo);
    };

    renderForm = () => {
        const layout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
        };

        const tailLayout = {
            wrapperCol: { offset: 6, span: 18 },
        };
        //const {show} = this.props;
        const {alertshow} = this.state;
        //console.log("####: show login form:" ,show);
        return (

            <Form
                {...layout}
                name="Login"
                initialValues={{ remember: true }}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
            >
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[{ required: true, message: 'Please input your email!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Пароль"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password />
                </Form.Item>

                {/* <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                    <Checkbox>Remember me</Checkbox>
                </Form.Item> */} 

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Войти
                    </Button>
                </Form.Item>

                <div className={cl(s.alert, {[s.show] : alertshow})}>Пользователь не найден!</div>
            </Form>
        )
    }

    render () {
        //console.log("####: props in login page:", this.props.props);
        return (
            <div className={cl(s.root, {
                [s.show] : this.props.show
            })}>
                <div className={s.formWrapper}>
                    {/*  <div className={s.choice}>
                        Войти
                    </div>  */}
                    <div className={s.formFields}>
                        {this.renderForm()}
                    </div>
                </div>    
            </div>
        );
    }
}

LoginPage.contextType = FirebaseContext;

export default LoginPage;