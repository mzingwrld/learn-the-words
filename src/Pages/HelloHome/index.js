import React, {Component} from 'react';
// import HeaderBlock from '../../components/HeaderBlock';
// import Header from '../../components/Header';
// import Paragraph from '../../components/Paragraph';
// import CardList from '../../components/CardList';
// import AccountPanel from '../../components/Account';
//import Footer from '../../components/Footer';
//import db from '../../services/firebase';

import s from './HelloHome.module.scss';
//import MainLogoPng from '../../images/logo-cc-1.png';
import {ReactComponent  as MainLogoSvg} from '../../images/logo-cc-1.svg'
//import TextContext from '../../context/testContext';
//import FirebaseContext from '../../context/firebaseContext';


class HelloPage extends Component {
    state = {
        loaded: false,
    }

    //urlRequest = `/cards/${this.props.user.uid}`;

    renderStage1 = () => {
        return (
            <>
                <div className={s.menu}>
                    <div className={s.menuItem}>Войти</div> { /* Show login */ }
                    <div className={s.menuItem}>Что это?</div> { /* Show registration demo */ }
                </div>
                <div className={s.wrap}> { /* dimensions: min 600x600 px // max 1200x1200 px */ }
                    <div className={s.main}>
                        Привет! 
                        <div className={s.anim1}>.</div>
                    </div>
                    <div className={s.anim2}>это</div>

                    <div className={s.anim3}><MainLogoSvg/></div> {/* LOGO */ }
                </div>
            </>
        )
    }

    render() {
        //const { wordArr } = this.state;
        //const { user } = this.props;
        //console.log("####: user", user.uid);
        //console.log("####: user", user.email);
        return (
            <div className={s.cover}>

                    {this.renderStage1()}

            </div>
          );
    }
}



export default HelloPage;