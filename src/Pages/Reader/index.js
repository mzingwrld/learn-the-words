import React, { Component } from 'react';
import getTranstalateWord from '../../services/translate';
import s from './Reader.module.scss';
import * as demoBooks from './demoBooks';
import { Tabs } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import FlexBoxContainer from '../../components/FlexBoxContainer';

import PopupTranslate from '../../components/TranslatePopup';
import prepareText from '../../services/prepareString';
import hash from 'object-hash';

const { TabPane } = Tabs;

const hobbit_title_1 = prepareText(demoBooks.HOBBIT_1);
const hobbit_title_2 = prepareText(demoBooks.HOBBIT_2);
const hobbit_book_p3 = prepareText(demoBooks.HOBBIT_3);
const hobbit_book_p4 = prepareText(demoBooks.HOBBIT_4);
const hobbit_book_p5 = prepareText(demoBooks.HOBBIT_5);
const potter_title = prepareText(demoBooks.POTTER_1);
const potter_book_p1 = prepareText(demoBooks.POTTER_2);
const potter_book_p2 = prepareText(demoBooks.POTTER_3);
const potter_book_p3 = prepareText(demoBooks.POTTER_4);
const potter_book_p4 = prepareText(demoBooks.POTTER_5);
const potter_book_p5 = prepareText(demoBooks.POTTER_6);

class Reader extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      book1ready: false,
      book1content: [],
      book1contentHash: null,
      book2ready: false,
      book2content: [],
      book2contentHash: null,
      x: 0, /* constantly reading coords */
      y: 0, /* constantly reading coords */
      coordsWhenClickedX: null,
      coordsWhenClickedY: null,
      showXpos: -401,
      showYpos: -401,
      popupX: null,
      popupY: null,
      size: 'small',
      showpopup: true,
      selectedwordid: null,
      word: null,
      translation: null,
      previousPopupShowPos: { 
        x: -400,
        y: -400,
      },
    };
  }

  componentDidMount () {
    document.addEventListener('mousedown', this.handleClick, false);

    this.renderHobbitBookContentAsync(hobbit_title_1, hobbit_title_2, hobbit_book_p3, hobbit_book_p4, hobbit_book_p5).then ( res => {
      const newHashbook1 = hash(res);
      if (newHashbook1 !== this.state.book1contentHash) {
        this.setState ( () => {
          return {
            book1ready: true,
            book1content: res,
          }
        })
      }
    });

    this.renderPotterBookContentAsync(potter_title, potter_book_p1, potter_book_p2, potter_book_p3, potter_book_p4, potter_book_p5).then ( res => {
      const newHashBook2 = hash(res);
      if (newHashBook2 !== this.state.book2contentHash) {
        this.setState ( () => {
          return {
            book2ready: true,
            book2content: res,
          }
        })
      }
    })
  }

  componentWillUnmount () {
    document.removeEventListener('mousedown', this.handleClick, false);
  }

  _onMouseMove(e) {
    this.setState({ 
      x: e.pageX, 
      y: e.pageY 
    });
  }

  handleSetPopupDimensions = (x, y) => {
    this.setState( () => {
      return { /* set dimensions of popup to state */
        popupX: x,
        popupY: y,
      }
    })
  }

  handleClick = (e) => {
    const {showpopup, showXpos, showYpos} = this.state;
    if(showpopup) {
      if(this.node.contains(e.target)) {
        console.log("####: Clicked inside");
      }
      else {
        // console.log("####: Clicked outside");
        // console.log("####: e.target.id", e.target.id);
        // console.log("####: this.state.selectedwordid", this.state.selectedwordid);
        // console.log("####: this.state", this.state);
        if (this.state.selectedwordid && e.target.id !== this.state.selectedwordid //&& showXpos < 0 && showYpos < 0
          ) {
          this.handleClosePopup();
        }
      } 
    }
  }

  handleTranslate = async (word, id) => {
    const getWord = await getTranstalateWord(word);
    //console.log("####: включи перед деплоем (перевод закостылен)");
    //const getWord = [{"text":"yellow","pos":"adjective","ts":"ˈjeləʊ","tr":[{"text":"желтый","pos":"adjective","syn":[{"text":"желтенький","pos":"adjective"}],"mean":[{"text":"amber"}],"ex":[{"text":"yellow card","tr":[{"text":"желтая карточка"}]},{"text":"yellow fever virus","tr":[{"text":"вирус желтой лихорадки"}]},{"text":"bright yellow color","tr":[{"text":"яркий желтый цвет"}]},{"text":"yellow school bus","tr":[{"text":"желтый школьный автобус"}]},{"text":"yellow police tape","tr":[{"text":"желтая полицейская лента"}]},{"text":"large yellow eyes","tr":[{"text":"большие желтые глаза"}]},{"text":"yellow legal pad","tr":[{"text":"желтый блокнот"}]},{"text":"yellow dwarf star","tr":[{"text":"желтый карлик"}]},{"text":"road of yellow brick","tr":[{"text":"дорога из желтого кирпича"}]}]},{"text":"желтокожий","pos":"adjective"}]},{"text":"yellow","pos":"noun","ts":"ˈjeləʊ","tr":[{"text":"желтый цвет","pos":"noun","mean":[{"text":"yellow color"}]},{"text":"желтизна","pos":"noun","gen":"ж","syn":[{"text":"пожелтение","pos":"noun","gen":"ср"}],"mean":[{"text":"yellowness"},{"text":"turn yellow"}]}]},{"text":"yellow","pos":"verb","ts":"ˈjeləʊ","tr":[{"text":"желтеть","pos":"verb","asp":"несов","syn":[{"text":"пожелтеть","pos":"verb","asp":"сов"}],"mean":[{"text":"turn yellow"}]}]}];
    //console.log("####: translate: ", getWord);
    setTimeout( () => {
      this.setState( () => {
        return {
          translation: getWord,
          selectedwordid: id,
          word: word,
        }
      });
    }, 300);
  }

  handleWordClick = (id, word) => { /* show popup over mouse position and set style for selected word */
    //console.log("####: handleWordClick fired, this.state", this.state);
    const { selectedwordid, x, y } = this.state;
    
    this.setState( () => { /* Store mouse position */
      return {
        coordsWhenClickedX: x,
        coordsWhenClickedY: y,
      }
    });


    if (selectedwordid) { /* remove style if some word have been selected */ 
      document.getElementById(selectedwordid).style.backgroundColor = null;
      document.getElementById(selectedwordid).style.color = null;
    }

    document.getElementById(id).style.backgroundColor = '#A239CA';
    document.getElementById(id).style.color = '#fff';

    /* 
      x - mouseX position      **   y - mouseY position
      popupX - width of popup  **   popupY - heigth of popup
    */
    if (id === selectedwordid) { /* Close popup if we clicked again on the same word */
      //console.log("#### handleWordClick->close popup fired");
      document.getElementById(selectedwordid).style.backgroundColor = null;
      document.getElementById(selectedwordid).style.color = null;

      this.setState( () => {
        return {
          selectedwordid: null,
          word: null,
          translation: 'nullg',
          showXpos: -400,
          showYpos: -400,
        }
      });
    } else {
      //console.log("#### handleWordClick->update with new word fired");
      this.handleTranslate(word, id).then( res => {
      })
    }
  }

  handleMovePopupOutside = () => {
    //console.log("#### handleMovePopupOutside fired");
    const {previousPopupShowPos, showXpos, showYpos} = this.state;

    if (showXpos !== previousPopupShowPos.x || showYpos !== previousPopupShowPos.y) {
      this.setState( () => {
        return {
          showXpos: -400,
          showYpos: -400,
          previousPopupShowPos: {
            x: -400,
            y: -400,
          }
        }
      });
    }
  }

  handleClosePopup = () => {
    //console.log("#### handleClosePopup fired");
    const {selectedwordid} = this.state;

    document.getElementById(selectedwordid).style.backgroundColor = null;
    document.getElementById(selectedwordid).style.color = null;

    this.setState( () => {
      return {
        showXpos: -400,
        showYpos: -400,
      }
    });
    //console.log("####: handleClosePopup state AFTER", this.state);
  }

  handleUpdatePopupPosition = (clx, cly) => {
    //console.log("#### handleUpdatePopupPosition fired", this.state);
    const { y, coordsWhenClickedX, coordsWhenClickedY } = this.state;
    //console.log ("#### compare data:");
    //console.log("#### clx",clx);
    //console.log("#### cly",cly);
    //console.log("####: handleUpdatePopupPosition state", this.state); 

    let popupWidth = document.getElementById('translatePopup').clientWidth;
    let popupHeight = document.getElementById('translatePopup').clientHeight;

    //console.log("####: handleUpdatePopupPosition popupWidth", popupWidth); 
    //console.log("####: handleUpdatePopupPosition popupHeight", popupHeight); 

    /* by default show popup under the word */
    //if (showXpos !== previousPopupShowPos.x || showYpos !== previousPopupShowPos.y) {
    //console.log("####: window.innerWidth;", window.innerWidth);
    //console.log("####: window.innerHeight;", window.innerHeight);
    //console.log("####: root div Height;", document.getElementById('root').clientHeight);

    let windowHeight = document.getElementById('root').clientHeight;

    if (popupHeight + coordsWhenClickedY > windowHeight) {
      //console.log("#### Коорд. места, где был клик + высота popup-а больше, чем высота окна");
      //console.log("#### popup снизу не влезет");
      //console.log("#### показываем popup сверху");
      this.setState( () => {
        return {
          showpopup: true,
          showXpos: coordsWhenClickedX - popupWidth/2,
          showYpos: coordsWhenClickedY - popupHeight - 20,
          previousPopupShowPos: {
            x : coordsWhenClickedX - popupWidth/2,
            y : coordsWhenClickedY - popupHeight - 20,
          },
        }
      });
    } else {
      //console.log("#### показываем popup снизу");
      this.setState( () => {
        return {
          showpopup: true,
          showXpos: coordsWhenClickedX - popupWidth/2,
          showYpos: coordsWhenClickedY + 20,
          previousPopupShowPos: {
            x : coordsWhenClickedX - popupWidth/2,
            y : coordsWhenClickedY + 20,
          },
        }
      });
    }
  }


  hasWhiteSpace = (s) => {
    return /\s/g.test(s);
  }

  renderBookContent = (content) => {
    let contentHash = hash(content);

    return ( 
    <div className={s.translatable} key={hash(content)}>
      {
        content.map ( ( firstLevel, index ) => {
          
          return firstLevel.map( ( { i, w, hash}) => {
            if (w.length > 1) { /* Words */
              return <p id={hash + i + index + contentHash } key={hash + i} onClick={() => {this.handleWordClick(hash + i + index + contentHash, w)} }> { w } </p>     
            }
            else if (w.length === 1 && /[A-Za-z]/gm.test(w)) { /* Articles and single letters */
              return <p id={hash + i + index + contentHash } key={hash + i} onClick={() => {this.handleWordClick(hash + i + index + contentHash, w)} }> { w } </p>
            }
            else if (this.hasWhiteSpace(w)) { /* Spaces */
              return <span key={hash + i + index + contentHash } className={s.textspace}> { w } </span>
            }
            else { /* Punctuations */
              return <span key={hash + i + index + contentHash }> { w } </span>
            }
          });
        })
      }
      </div>
    )
  }

  renderHobbitBookContentAsync = async (c1,c2,c3,c4,c5) => {
    return (
      <FlexBoxContainer directionCol>
      <div className={s.bookCoverhobbit}></div>
      <div className={s.translatableTitle} onMouseMove={this._onMouseMove.bind(this)}>
    
          <div className={s.bookTitle}> { this.renderBookContent(c1) } </div>
          <div className={s.bookTitle}> { this.renderBookContent(c2) } </div>
        
          { this.renderBookContent(c3) }
          { this.renderBookContent(c4) }
          { this.renderBookContent(c5) }
      </div>
      </FlexBoxContainer> 
    )
  }

  renderPotterBookContentAsync = async (c1,c2,c3,c4,c5,c6) => {
    return (
      <FlexBoxContainer directionCol>
      <div className={s.bookCoverhp}></div>
      <div onMouseMove={this._onMouseMove.bind(this)}>
        <div className={s.translatable}>
          <div className={s.bookTitle}> {this.renderBookContent(c1)}</div>
        
          { this.renderBookContent(c2) }
          { this.renderBookContent(c3) }
          { this.renderBookContent(c4) }
          { this.renderBookContent(c5) }
          { this.renderBookContent(c6) }
          
        </div>
      </div>
    </FlexBoxContainer> 
    )
  }

  render() {
    const { size, showpopup, showXpos, showYpos, word, translation } = this.state;
    return (
      <div>
        <div ref={ node => this.node = node }>
        <PopupTranslate 
          showXpos={showXpos}
          showYpos={showYpos}
          show={showpopup}
          word={word}
          translation={translation}
          onComponentDidMountCustom={this.handleSetPopupDimensions}
          onClickCustom={this.handleClosePopup}
          handleUpdatePopupPositionTrigger={this.handleUpdatePopupPosition}
          handleMovePopupOutsideTrigger={this.handleMovePopupOutside}
        />
        </div>
        <FlexBoxContainer directionCol>
          <Tabs defaultActiveKey="1" type="card" size={size}>
            <TabPane tab={demoBooks.HOBBIT_TITLE} key="1">
              <div className={s.plaintext}>
                { this.state.book1ready ? this.state.book1content : <div className={s.loading}><LoadingOutlined spin /></div> }
              </div>
            </TabPane>
            <TabPane tab={demoBooks.POTTER_TITLE} key="2">
              <div className={s.plaintext}>
                { this.state.book2ready ? this.state.book2content : <div className={s.loading}><LoadingOutlined spin /></div> }
              </div>
            </TabPane>
          </Tabs>
        </FlexBoxContainer>
      </div>
    );
  }
}

 
export default Reader;