import React, {Component} from 'react';
import HeaderBlock from '../../components/HeaderBlock';
import Header from '../../components/Header';
import Paragraph from '../../components/Paragraph';
import CardList from '../../components/CardList';
import AccountPanel from '../../components/Account';
import Footer from '../../components/Footer';
//import db from '../../services/firebase';

//import s from './Home.module.scss';


import FirebaseContext from '../../context/firebaseContext';

import { connect } from 'react-redux';
import * as actions from '../../actions';
import { bindActionCreators } from 'redux';

import { cardListActionAPI, cardListAction, cardListResolveAction, cardListRejectAction, addWordAction, deleteWordAction } from '../../actions/cardListAction';


var history = [];
var sequence = 0;
/*
const fetchCardListAPI = (getData, hideLoader, prepareArr, dispatch) => {
            getData().once('value').then(res => {
                hideLoader();
                dispatch(cardListResolveAction(prepareArr(res.val())));
            }).catch(err => {
                dispatch(cardListRejectAction(err));
            });
} */

class HomePage extends Component {

    handlePrepareArray = (response) => {
        var preparedArray = [];
        console.log("####: get user res val:", response);
                if(response !== null) {
                    preparedArray = response.filter(function (e) {
                    return e !== null;
                    });
                    sequence = Math.max(...preparedArray.map(item => item.id)) + 1; /* get max id from db */
                    
                } else {
                    sequence = 0;
                }
        return preparedArray;
    }

    componentDidMount() {
        const { getUserCardsRef, auth
            //, setUserUid 
        } = this.context;
        const {
            fetchCardList,
            fetchCardListAPI,
            dispatch,
        } = this.props;

        console.log("####: componentDidMount HomePage props", this.props);
        
        fetchCardList();

        auth.onAuthStateChanged( (user) => {
            if (user) {
            console.log("####: Auth state changed at HOMEPAGE", user);
            //setUserUid(user.uid); // context
            fetchCardListAPI(getUserCardsRef, this.handleHideLoader, this.handlePrepareArray, dispatch);
/*             getUserCardsRef().once('value').then(res => {
                this.handleHideLoader();
                fetchCardListResolve(this.handlePrepareArray(res.val()));
            }).catch(err => {
                fetchCardListReject(err);
            }); */
            }
        });
    }

    componentDidUpdate() {
        console.log("####: componentDidUpdate HomePage");
    }

    componentWillUnmount() {

        console.log("####: componentWillUnmount HomePage");
        this.handleHideLoader();

    }


    handleHideLoader = () => {
        console.log("####: setstate #2");
        this.setState( () => {
            return {
                loaded: true
            }
        })
    }
    
    handleAddItemToDB = (wordObj) => {
        const { getUserCardsRefToManageCards } = this.context;
        console.log("####: before adding to db:", wordObj);
        console.log("####: url for db:", this.urlRequest + '/' + wordObj[0].id);
        return getUserCardsRefToManageCards(wordObj[0].id).set(wordObj[0]);
    }

    handleAddItem = (word1, word2, lang) => {
        const { wordArr } = this.props;
        const {
            addWord,
        } = this.props;
        console.log("####: wordArr in handleAddItem: ", wordArr);

        //const maxId = Math.max(...wordArr.map(item => item.id)) + 1;
        //console.log("####: idx ", maxId);
        history = wordArr;
        
        let newWord = [];
        if (lang === 'en-ru') {
            //this.handleAddItemToDB(word1, word2, sequence);
            newWord = [{
                eng: word1,
                rus: word2,
                id: sequence
            }];
            //sequence ++;
        }
        else if (lang === 'ru-en') {
            //this.handleAddItemToDB(word2, word1, sequence);
            newWord = [{
                eng: word2,
                rus: word1,
                id: sequence
            }];
            //sequence ++;
        }
        else { //default en-ru:
            //this.handleAddItemToDB(word2, word1, sequence);
            newWord = [{
                eng: word2,
                rus: word1,
                id: sequence
            }];
            //sequence ++;
        }
        console.log("####: sequence after insert ", sequence);
        console.log("####: word1: ", word1);
        console.log("####: word2: ", word2);
        console.log("####: lang:", lang);
        
        //console.log("####: state before", this.state.wordArr);
        const newWordArr = [
            ...wordArr,
            ...newWord
        ];
        //console.log("####: state after", this.state.wordArr);
        //console.log("####: new Word:", newWord);
        //console.log("####: new Word[0]:", newWord[0]);
        this.handleAddItemToDB(newWord).then( () => {
        //this.handleAddItemToDB(newWord[0]).then(
            sequence++;
            addWord(newWordArr, 'added', newWord[0]);
            
           /* this.setState( () => {
                sequence++;
                console.log("####: sequence after add to DB", sequence);
                return {
                    wordArr: newWordArr,
                    previousstate: 'added',
                    addeditem: newWord[0]
                }
            })*/
        });
    }

    handleDeleteItemFromDB = (id) => {
        const { getUserCardsRefToManageCards } = this.context;
        return getUserCardsRefToManageCards(id).remove();

    }

    handlerDeletedItem = (id) => {
        const {wordArr, deleteWord} = this.props;
        const idx = wordArr.findIndex(item => item.id === id);
        history = wordArr[idx]; 
        
        console.log("####: idx ", idx);
        //console.log("####: state before", this.state.wordArr);
        const newWordArr = [
            ...wordArr.slice(0, idx),
            ...wordArr.slice(idx + 1)
        ];
        //console.log("####: state after", this.state.wordArr);
        this.handleDeleteItemFromDB(id).then( () => {
            console.log("####: sequence after deleting from db: ", sequence);
            deleteWord(newWordArr, 'deleted', history);            
        }
/*             this.setState( () => {
                console.log("####: sequence after deleting from db: ", sequence);
                return {
                    wordArr: newWordArr,
                    previousstate: 'deleted',
                    deleteditem: history
                }
            }) */
        );
    }
    
    handleRevertChange = () => {
        const { previousstate, addeditem, deleteditem } = this.props;
        //console.log("####: готовы вернуть как было");
        
        if (previousstate === 'deleted') {
            //console.log("####: прошлое действие было deleteItem" );
            //console.log("####: deleteditem ", deleteditem);

            this.handleAddItem(deleteditem.rus, deleteditem.eng);
        }
        if (previousstate === 'added') {
            //console.log("####: прошлое действие было addItem")
            //console.log("####: addeditem ", addeditem);

            this.handlerDeletedItem(addeditem.id);
        }
    }

    render() {
        //console.log("####: Home page")
        //console.log("####: props at homePage [redux]", this.props);
/*         const {
            plusAction,
            minusAction,
            countNumber
        } = this.props; */

        console.log("####: Render HomePage");
        const { wordArr, loaded } = this.props;
        console.log("####: wordArr and loaded", loaded, wordArr);
        console.log("####: HomePage props", this.props);
        console.log("####: HomePage context", this.context );

        const { email } = this.context;

        return (
            <>
                {  <AccountPanel useremail={email || "..выполняем вход"} props={this.props}/>  }
                <HeaderBlock>
                    <Header>
                        Время учить слова онлайн
                    </Header>
                    <Paragraph>
                        Используйте карточки для запоминания и пополняйте активный словарный запас.
                    </Paragraph>
                </HeaderBlock>

                <CardList 
                item = {wordArr} 
                onDeletedItem={this.handlerDeletedItem}
                onAddItem={this.handleAddItem}
                onRevertChange={this.handleRevertChange}
                loading = {loaded || false}
                />

{/*                 <HeaderBlock hideBackground>
                    <Header>
                        Redux test
                    </Header>
                    <Header>
                        { countNumber }
                    </Header>
                    <Button onClick={() => plusAction(1)}>PLUS</Button>
                    <Button onClick={() => minusAction(1)}>MINUS</Button>
                    <Paragraph>
                        Ну здорово же!
                    </Paragraph>
                    <ReactLogoSvg/>
                </HeaderBlock> */}

{/*                 <HeaderBlock spaceBackground>
                    <Header>
                        Космический заголовок
                    </Header>
                    <Paragraph>
                        На фоне тоже космос, кстати
                    </Paragraph>
                    <ReactLogoSvg/>
                </HeaderBlock> */}

                <Footer /> 
            </>
          );
    }
}

HomePage.contextType = FirebaseContext;

const mapStateToProps = (state, ownProps) => {
    console.log("####: mapStateToProps[state] HomePage:", state);
    console.log("####: mapStateToProps[ownProps] HomePage:", ownProps);
    return  {
        ...state,
        wordArr: state.cardList.wordArr || [],
        countNumber: state.counter.count,
        previousstate: state.cardList.previousstate,
        deleteditem: state.cardList.deleteditem,
        addeditem: state.cardList.addeditem,
        loaded: state.cardList.loaded,
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        dispatch,
        addWord: addWordAction,
        deleteWord: deleteWordAction,
        fetchCardListAPI: cardListActionAPI,
        fetchCardList: cardListAction,
        fetchCardListResolve: cardListResolveAction,
        fetchCardListReject: cardListRejectAction,
        plusAction: actions.plusAction,
        minusAction: actions.minusAction,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);