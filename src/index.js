import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { Provider  } from 'react-redux';
import { createStore
    //, bindActionCreators
    , applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import rootReducers from './reducers';

import 'antd/dist/antd.css';
import './index.css';
import FirebaseContext from './context/firebaseContext';
import Firebase from './services/firebase';
import { BrowserRouter } from 'react-router-dom';


/* LOG INFO TROUGH CHROME EXTENSION :*/
//const store = new createStore(rootReducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
/* just to commit */
/* LOG INFO TROUGH REACT LOGGER:*/
const store = new createStore(rootReducers, applyMiddleware(thunk, logger));

/*console.log ('####: store', store.getState());

const { dispatch } = store;
const { plusAction, minusAction } = bindActionCreators(actions, dispatch);

store.subscribe(() => console.log("####: subscr store getState:", store.getState())); 

plusAction(5);
minusAction(3);*/

ReactDOM.render(
                <Provider store={store}>
                    <FirebaseContext.Provider value = {new Firebase()} >
                        <BrowserRouter>
                            <App />
                        </BrowserRouter>
                    </FirebaseContext.Provider>
                </Provider>
                , document.getElementById('root')
            );