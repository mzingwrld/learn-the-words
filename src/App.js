import React, {Component} from 'react';
import { connect } from 'react-redux';

import HomePage from './Pages/Home';
import HelloPage from './Pages/HelloHome';
import AuthSwitch from './Pages/AuthSwitch';
import Reader from './Pages/Reader';

import TestContext from './context/testContext';
import FirebaseContext from './context/firebaseContext';

import { LoadingOutlined } from '@ant-design/icons';
import s from './App.module.scss';

import { Route, Link, Redirect, Switch } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import { bindActionCreators } from 'redux';
import * as actions from './actions/userAction';

const { Header, Content } = Layout;

class App extends Component {
    state = {
        loaded: false,
    };


    componentDidMount() {
        const { auth, setUserUid, setUser, setUserEmail } = this.context;
        const { addUser, signOutUser } = this.props;

        auth.onAuthStateChanged( (user) => {
            console.log("####: Auth state changed at App.js");
            console.log("####: user:", user);

            if (user) {
                addUser(user); // redux

                setUserUid(user.uid); // context
                setUser(user); // context
                setUserEmail(user.email); // context

                
            } else {

                signOutUser(); // redux

                setUserUid(null); // context
                setUser(null); // context
                setUserEmail(null); // context

            }
        });
    }

    handleHideLoader = () => {
        const { loaded } = this.state;
        console.log("####: App.js loaded:", loaded);
        //let timeOut = 
        setTimeout(() => {
            this.setState({
                loaded: true,
            });
        }, 1000);
    }

    render() {
        const { user } = this.props;
        const { loaded } = this.state; 
        console.log("####: App.js user:", user);
        if (!loaded) {
            this.handleHideLoader();
            return (
                <div className={s.loading}>
                    <LoadingOutlined spin />
                </div>
            )
        }

        return (
            <>
                    <TestContext.Provider value={{user}}>
                        

                            { !user.user
                                ? 
                                <Route 
                                    render = { () => {
                                        return (
                                            <Switch>
                                                <Route path='/hello' exact component={HelloPage} /> 
                                                <Route path='/login' exact component={AuthSwitch} />
                                                <Route  path='/reader' exact component={Reader} />
                                                <Redirect to="/login" />
                                            </Switch>
                                        );
                                    }}
                                />
                                : 
                                <></> 
                            }

                            { user.user
                                ?
                                <Route render = { (props) => {
                                console.log("####: props", props);
                                const {history : {push}} = props;
                                return (
                                    <Layout>
                                        <Header>
                                             <Menu theme="dark" mode="horizontal">  
                                                <Menu.Item key="1">
                                                    <Link to="/">Главная</Link>
                                                </Menu.Item>
                                                <Menu.Item key="2">
                                                    <Link to="/about">О проекте</Link>
                                                </Menu.Item>
                                                <Menu.Item key="3" onClick={() => push('/contact')}>
                                                    Контакты
                                                </Menu.Item>                                            
                                            </Menu>
                                        </Header>
                                        <Content>
                                                <Switch>
                                                    <Route path='/home' exact component = { HomePage } />
                                                    <Route path='/about' exact render={ () => <h1>This is about something..</h1>} />
                                                    <Route path='/contact' exact render={ () => <h1>This is contact page..</h1>} />
                                                    <Redirect to="/home" />
                                                </Switch>
                                        </Content>
                                    </Layout>
                            );
                            }} /> :  <></> }
                    </TestContext.Provider>
            </>
          );
    }
}

App.contextType = FirebaseContext;

const mapStateToProps = (state, ownProps) => {
    console.log("####: mapStateToProps[state] App.js:", state);
    console.log("####: mapStateToProps[ownProps] App.js:", ownProps);
    return {
        ...state,
        user: state.user,
    }
}

const mapDispatchToProps = (dispatch) => {

    return bindActionCreators({
        ...actions,
        addUser: actions.addUserAction,
        signOutUser: actions.signOutUserAction,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
