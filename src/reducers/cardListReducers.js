import { FETCH_CARD_LIST, FETCH_CARD_LIST_REJECT, FETCH_CARD_LIST_RESOLVE, FIREBASE_ADD_ITEM, FIREBASE_DELETE_ITEM } from "../actions/actionTypes"

const cardListReducer = (state, action) => {
    console.log("####: cardListReducer state", state);
    console.log("####: cardListReducer action", action);
    switch (action.type) {
        case FETCH_CARD_LIST:
            return {
                ...state,
                wordArr: [],
                previousstate: '',
                deleteditem: [],
                addeditem: [],
                loaded: false,
                err: null,            
            }

        case FETCH_CARD_LIST_RESOLVE:
            return {
                ...state,
                wordArr: action.payload,
                previousstate: '',
                deleteditem: [],
                addeditem: [],
                loaded: true,
                err: null,            
            }
        case FETCH_CARD_LIST_REJECT:
            return {
                ...state,
                wordArr: [],
                previousstate: '',
                deleteditem: [],
                addeditem: [],
                loaded: true,
                err: action.err,            
            }
        case FIREBASE_ADD_ITEM:
            return {
                ...state,
                wordArr: action.payload,
                previousstate: action.previousstate,
                addeditem: action.addeditem,
                loaded: true,
                err: null,
            }
        case FIREBASE_DELETE_ITEM:
            return {
                ...state,
                wordArr: action.payload,
                previousstate: action.previousstate,
                deleteditem: action.deleteditem,
                loaded: true,
                err: null,
            }
        default: 
            return {
                ...state,      
            }
    }
}

export default cardListReducer;