import { PLUS, MINUS } from "../actions/actionTypes";

const counterReducer = (state = {count: 0}, action) => {
    switch(action.type) {
        case PLUS:
            return {
                ...state,
                count: state.count + action.amount,
            }
        case MINUS:
            return {
                ...state,
                count: state.count - action.amount,
            }
        default:
            return {
                ...state,
            }
    }
}

export default counterReducer;