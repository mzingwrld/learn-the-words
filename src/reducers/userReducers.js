import { ADD_USER, SIGN_OUT_USER } from "../actions/actionTypes";

const userRecucer = (state, action) => {
    console.log("####: userRecucer state", state);
    console.log("####: userRecucer action", action);
    switch (action.type) {
        case ADD_USER:
            return {
                ...state,
                user: action.user,
            }
        case SIGN_OUT_USER:
            return {
                ...state,
                user: null,
            }
        default: 
            return {
                ...state,
            }
    }
}

export default userRecucer;