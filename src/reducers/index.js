import { combineReducers } from 'redux';
import counterReducer from './counterReducers';
import userRecucer from './userReducers';
import cardListReducer from './cardListReducers';


export default combineReducers({
    user: userRecucer,
    counter: counterReducer,
    cardList: cardListReducer,
});