import { ADD_USER, SIGN_OUT_USER } from "./actionTypes";

export const addUserAction = (user) => ({
    type: ADD_USER,
    user,
})

export const signOutUserAction = () => ({
    type: SIGN_OUT_USER,
})