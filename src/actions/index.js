import { PLUS, MINUS } from "./actionTypes"

export const plusAction = (amount) => {
    return {
        type: PLUS,
        amount: amount,
    }
}

export const minusAction = (amount) => {
    return {
        type: MINUS,
        amount: amount,
    }
}