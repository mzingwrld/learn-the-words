import { FETCH_CARD_LIST, FETCH_CARD_LIST_RESOLVE, FETCH_CARD_LIST_REJECT, FIREBASE_ADD_ITEM, FIREBASE_DELETE_ITEM } from "./actionTypes";

export const cardListActionAPI = (getData, hideLoader, prepareArr) => {
    return (dispatch, getState) => {
        getData().once('value').then(res => {
            hideLoader();
            dispatch(cardListResolveAction(prepareArr(res.val())));
        }).catch(err => {
            dispatch(cardListRejectAction(err));
        });
    }
}

export const cardListAction = () => ({
    type: FETCH_CARD_LIST,
});

export const cardListResolveAction = (payload) => ({
    type: FETCH_CARD_LIST_RESOLVE,
    payload,
});

export const cardListRejectAction = (err) => ({
    type: FETCH_CARD_LIST_REJECT,
    err,

});

export const addWordAction = (payload, previousstate, addeditem) => ({
    type: FIREBASE_ADD_ITEM,
    payload,
    previousstate,
    addeditem,
});

export const deleteWordAction = (payload, previousstate, deleteditem) => ({
    type: FIREBASE_DELETE_ITEM,
    payload,
    previousstate,
    deleteditem,
});