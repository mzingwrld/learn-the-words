import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
    authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
    projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
    storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_FIREBASE_APP_ID
}

class Firebase {
    constructor() {
        firebase.initializeApp(firebaseConfig);

        this.auth = firebase.auth();
        this.db = firebase.database();

        this.userUid = null;
        this.user = null;

        this.email = null;
    }

    setUserUid = (uid) => {
        this.userUid = uid;
    }

    setUser = (user) => this.user = user;

    setUserEmail = (email) => {
        this.email = email;
    };

    signInWithEmail = (email, password) => 
        this.auth.signInWithEmailAndPassword(email, password);
    
    registerWithEmail = (email, password) => 
        this.auth.createUserWithEmailAndPassword(email, password);
    
    //getUserCardsRef = (link) => this.db.ref(link);
    getUserCardsRef = () => this.db.ref(`/cards/${this.userUid}`);

    getUserCardsRefToManageCards = (id) => this.db.ref(`/cards/${this.userUid}/${id}`);

    sendVerificationEmail = () => this.auth.currentUser.sendEmailVerification();
}




export default Firebase;