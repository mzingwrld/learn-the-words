import tokenizeString from './tokenize';

const splitStringBySentences = (str) => {
    let result = str.replace(/([.?!;])\s*(?=[A-Z])/g, "$1| ").split("|");
    return result;
}

const prepareText = (str) => {
    let splitPrepared = splitStringBySentences(str);
    let splitResult = []
    splitPrepared.forEach( sentence => (
      splitResult.push(sentence)
    ));
    
    let tokeniziedResult = [];

    splitResult.forEach ( sentence => (
      tokeniziedResult.push(tokenizeString(sentence))
    ));

    return tokeniziedResult;
}

export default prepareText;
//export default splitStringBySentences;