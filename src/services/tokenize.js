import hash from 'object-hash';

const tokenizeString = (str)  => {

    //var punct = '\\[' + '\\!' + '\\"' + '\\#' + '\\$'  + // since javascript does not
    //            '\\%' + '\\&' + '\\\'' + '\\(' + '\\)' + // support POSIX character
    //            '\\*' + '\\+' + '\\,' + '\\\\' + '\\-' + // classes, we'll need our
    //            '\\.' + '\\/' + '\\:' + '\\;' + '\\<'  + // own version of [:punct:]
    //            '\\=' + '\\>' + '\\?' + '\\@' + '\\['  + 
    //            '\\]' + '\\^' + '\\_' + '\\`' + '\\{'  + 
    //            '\\|' + '\\}' + '\\~' + '\\]' + '\\s*' + '\\—',
    var punct = '\\[\\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\\\\\-\\.\\/\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\`\\{\\|\\}\\~\\]\\s*\\—',
        re = new RegExp( // tokenizer
            //'\\s*' +           // discard possible leading whitespace
            '(' +              // start capture group #1
                '\\.{3}' +          // ellipsis (must appear before punct)
            '|' +              // alternator
                '\\w+\\-\\w+' +     // hyphenated words (connected with -) (must appear before punct)
            '|' +              // alternator
                '\\w+\'(?:\\w+)?' + // compound words (must appear before punct)
            '|' +              // alternator
                '\\w+' +            // other words
            '|' +              // alternator
                '[' + punct + ']' +  // punct
            ')' // end capture group #1
          ),
        tokens=str.split(re),  // split string using tokenizing regex
        result=[];
    
    // add non-empty tokens to result
    for(var i=0,len=tokens.length;i++<len;) {
        if(tokens[i]) {
            result.push({
                i: i,
                w: tokens[i],
                hash: hash(tokens[i])
                }
            );
        }
    };

    
    return result;

} // end tokenize()

export default tokenizeString;