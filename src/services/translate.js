const keyYandex = process.env.REACT_APP_YANDEX_API_KEY;
//console.log("####: env", process.env);

const getTranstalateWord = async (text, lang= 'en-ru') => {
    const res = await fetch(`https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=${keyYandex}&lang=${lang}&text=${text}`);
    const body = await res.json();
    
    return body.def || [];
}


export default getTranstalateWord;